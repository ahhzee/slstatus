#!/bin/sh

[ -f "./config.h" ] && rm -f config.h
sudo make install
[ "$1" = "r" ] && killall slstatus && setsid -f slstatus > /dev/null
